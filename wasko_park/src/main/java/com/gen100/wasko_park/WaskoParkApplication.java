package com.gen100.wasko_park;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaskoParkApplication {

	public static void main(String[] args) {
		SpringApplication.run(WaskoParkApplication.class, args);
	}

}
